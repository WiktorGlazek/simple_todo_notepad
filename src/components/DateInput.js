import React, { Fragment } from 'react';

const DateInput = ({noteDate, setNoteDate}) => {
    return (
        <Fragment>
            <input type="date" value={noteDate} onChange={ e => setNoteDate(e.target.value)}/>
        </Fragment>
    );
};

export default DateInput;