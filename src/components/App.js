import React, { useState } from 'react';
import Form from './Form';

const App = () => {

    const [notes, setNotes] = useState([])
    const [errors, setErrors] = useState([])

    const deleteNote = index => {
        setNotes(notes.filter((note, noteIndex)=> noteIndex !== index))
    }

    return (
        <div className='notes'>
            <h1>TODO notepad</h1>
            <Form notes={notes} setNotes={setNotes} setErrors={setErrors}/>
            <div>
                {errors.length === 0 ? <></> : errors.map(
                        (error, index )=>(
                            <div className="error" key={index}>{error}</div>
                        )
                    )
                }
            </div>
            <h2>Your notes:</h2>
            {
                notes.map(
                    (note, index )=>(
                        <div key={index} className="note">
                            {note.noteText} --- {note.noteDate}
                            <div onClick={()=> deleteNote(index)} className= "o"></div>
                        </div>
                    )
                )
            }
        </div>
    );
};

export default App;