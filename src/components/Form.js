import React, {useState} from 'react';
import TextInput from './TextInput';
import DateInput from './DateInput';

const Form = ({notes, setNotes, setErrors}) => {

    const [noteText, setNoteText] = useState("")
    const [noteDate, setNoteDate] = useState("")

    const addNote = () => {
        const currErrors = []
        if (noteText === ""){
            currErrors.push("Add text")
        }
        if (noteDate === ""){
            currErrors.push("Add date")
        }
        else {
            const today = new Date().setHours(0,0,0,0)
            const deadline = new Date(noteDate).setHours(0,0,0,0)
            if (today >= deadline){
                currErrors.push("Past date")
            }
        }
        if (currErrors.length === 0){
            setNotes([...notes, {noteText, noteDate}])
            setNoteText("")
            setNoteDate("")
            setErrors([])
        }
        else setErrors(currErrors)
    }

    return (
        <div>
            <TextInput noteText={noteText} setNoteText={setNoteText}/>
            <DateInput noteDate={noteDate} setNoteDate={setNoteDate}/>
            <button onClick={addNote}>Add note</button>
        </div>
    );
};

export default Form;