import React, { Fragment } from 'react';

const TextInput = ({noteText, setNoteText}) => {
    return (
        <Fragment>
            <input type="text" placeholder='note' value={noteText} onChange={ e => setNoteText(e.target.value)}/>
        </Fragment>
    );
};

export default TextInput;